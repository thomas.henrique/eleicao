CREATE DATABASE IF NOT EXISTS eleicao;
USE eleicao;

CREATE TABLE IF NOT EXISTS candidatos(
	id INT PRIMARY KEY AUTO_INCREMENT,
    numero_cadidato INT NOT NULL UNIQUE,
    nome_cadidato VARCHAR(50),
    votos INT
);

CREATE TABLE IF NOT EXISTS estudantes(
	id INT PRIMARY KEY AUTO_INCREMENT,
    nome_estudante VARCHAR(50),
    email VARCHAR(50),
    senha VARCHAR(100)
);
